﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Options;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Encodings.Web;
using BasicAuthApi.Utilities;
using BasicAuthApi.Utilities.Interfaces;
using System.Security.Claims;

namespace BasicAuthApi.Security
{
    public class BasicAuthHandler : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        private readonly IMyUtilities _utils;

        public BasicAuthHandler(IOptionsMonitor<AuthenticationSchemeOptions> options, 
            ILoggerFactory logger, 
            UrlEncoder encoder, 
            ISystemClock clock, 
            IMyUtilities utils
        ) : base(options, logger, encoder, clock)
        {
            _utils = utils;
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (!Request.Headers.ContainsKey("Authorization"))
                return AuthenticateResult.Fail("Invalid Header");
            
            bool result = false;
            try
            {
                var autHeader = AuthenticationHeaderValue.Parse(Request.Headers["Authorization"]);
                var credentialsBytes = Convert.FromBase64String(autHeader.Parameter??"");
                var credentials = Encoding.UTF8.GetString(credentialsBytes).Split(new[] {':'}, 2);
                var user = credentials[0];
                var pwd = credentials[1];
                result = _utils.IsUser(user, pwd);

            } catch
            {
                return AuthenticateResult.Fail("Invalid Header");
            }

            if (!result)
                return AuthenticateResult.Fail("Invalid credentials");

            var claims = new Claim[]
            {
                new Claim(ClaimTypes.NameIdentifier, "id"),
                new Claim(ClaimTypes.Name, "user")
            };

            var identity = new ClaimsIdentity(claims, Scheme.Name);
            var principal = new ClaimsPrincipal(identity);
            var ticket = new AuthenticationTicket(principal, Scheme.Name);
            return AuthenticateResult.Success(ticket);
        }
    }
}
