using Newtonsoft.Json;
using System.Net;
using RestSharp;

namespace BasicAuthApi.Utilities
{
    class MyHttp<T>
    {
        private string _urlBase { get; set; }
        private string _endpoint { get; set; }
        private readonly Dictionary<string, string> _headers;

        public MyHttp(string urlBase, string endpoint, Dictionary<string, string> headers)
        {
            _urlBase = urlBase;
            _endpoint = endpoint;
            _headers = headers;
        }

        public T? Get()
        {
            var client = new RestClient(_urlBase);
            var request = new RestRequest(_endpoint);
            foreach (var header in _headers)
            {
                request.AddHeader(header.Key, header.Value);
            }
            var response = client.Execute(request);
            if (!response.IsSuccessful) throw new Exception(response.Content);
            var data = JsonConvert.DeserializeObject<T>(response.Content ?? "");
            return data;
        }

        public T? Post(object body)
        {
            var client = new RestClient(_urlBase);
            var request = new RestRequest(_endpoint, Method.Post);
            foreach (var header in _headers)
            {
                request.AddHeader(header.Key, header.Value);
            }
            request.AddJsonBody(body);
            var response = client.Execute(request);
            if (!response.IsSuccessful) throw new Exception(response.Content);
            var data = JsonConvert.DeserializeObject<T>(response.Content ?? "");
            return data;
        }

        public T? Put(object body)
        {
            var client = new RestClient(_urlBase);
            var request = new RestRequest(_endpoint, Method.Put);
            foreach (var header in _headers)
            {
                request.AddHeader(header.Key, header.Value);
            }
            request.AddJsonBody(body);
            var response = client.Execute(request);
            if (!response.IsSuccessful) throw new Exception(response.Content);
            var data = JsonConvert.DeserializeObject<T>(response.Content ?? "");
            return data;
        }
    }
}