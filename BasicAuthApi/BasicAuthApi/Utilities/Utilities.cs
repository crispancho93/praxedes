﻿using BasicAuthApi.Utilities.Interfaces;

namespace BasicAuthApi.Utilities
{
    public class MyUtilities : IMyUtilities
    {
        private readonly IConfiguration _config;

        public MyUtilities(IConfiguration config)
        {
            _config = config;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="pwd"></param>
        /// <returns></returns>
        public bool IsUser(string user, string pwd)
        {
            var result = false;
            var dbUser = _config["BarrerUser:User"];
            var dbPwd = _config["BarrerUser:Pwd"];
            if (!string.IsNullOrEmpty(dbUser) && !string.IsNullOrEmpty(dbPwd))
            {
                if (dbUser == user && dbPwd == pwd)
                {
                    result = true;
                }
            }
            return result;
        }
    }
}
