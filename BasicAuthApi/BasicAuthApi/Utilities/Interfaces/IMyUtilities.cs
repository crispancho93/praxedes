﻿namespace BasicAuthApi.Utilities.Interfaces
{
    public interface IMyUtilities
    {
        bool IsUser(string user, string pwd);
    }
}
