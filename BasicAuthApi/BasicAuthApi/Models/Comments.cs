﻿namespace BasicAuthApi.Models
{
    public class Comments
    {
        public int Id { get; set; }
        public int PostId { get; set; }
        public string Name { get; set; } = null!;
        public string Email { get; set; } = null!;
        public string Body { get; set; } = null!;
    }
}
