﻿using BasicAuthApi.Models;
using BasicAuthApi.Repository.Interfaces;
using BasicAuthApi.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BasicAuthApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PostsController : ControllerBase
    {
        private readonly IPostsRepository _repository;

        public PostsController(IPostsRepository repository)
        {
            _repository = repository;
        }

        // GET: api/<PostsController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Posts>>> Get()
        {
            try
            {
                var data = await _repository.GetAll();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(new { msg = ex.Message });
            }
        }

        // GET api/<PostsController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Posts>> Get(int id)
        {
            try
            {
                var data = await _repository.GetById(id);

                if (data is null)
                {
                    return NotFound(new { msg = "Data NotFound" });
                }

                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(new { msg = ex.Message });
            }
        }

        // POST api/<PostsController>
        [HttpPost]
        public async Task<ActionResult<int>> Post([FromBody] Posts posts)
        {
            try
            {
                var affected = await _repository.Upsert(posts);
                if (affected is not 1)
                {
                    return NotFound(new { msg = "Data NotFound" });
                }
                return Ok(new { msg = $"affected_rows {affected}" });
            }
            catch (Exception ex)
            {
                return BadRequest(new { msg = ex.Message });
            }
        }

        // PUT api/<PostsController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult<int>> Put(int id, [FromBody] Posts posts)
        {
            try
            {
                posts.Id = id;
                var affected = await _repository.Upsert(posts);
                if (affected is not 1)
                {
                    return NotFound(new { msg = "Data NotFound" });
                }
                return Ok(new { msg = $"affected_rows {affected}" });
            }
            catch (Exception ex)
            {
                if (ex.Message is "Data NotFound")
                    return NotFound(new { msg = "Data NotFound" });
                else
                    return BadRequest(new { msg = ex.Message });
            }
        }

        // POST api/<PostsController>/Populate
        [HttpPost]
        [Route("Populate")]
        public ActionResult Populate()
        {
            try
            {
                // Get all posts
                var urlBase = "https://jsonplaceholder.typicode.com";
                var endpoint = "/posts";
                var headers = new Dictionary<string, string>()
                {
                    {"Accept", "application/json"},
                    {"Content-Type", "application/json"},
                };
                var posts = new MyHttp<List<Posts>>(urlBase, endpoint, headers).Get();

                if (posts is null)
                {
                    return NotFound(new { msg = "Data NotFound" });
                }

                // Insert posts
                foreach (var post in posts)
                {
                    _repository.Upsert(post);
                }

                return Ok(new { msg = "ok" });
            }
            catch (Exception ex)
            {
                return BadRequest(new { msg = ex.Message });
            }
        }

        // DELETE api/<PostsController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<int>> Delete(int id)
        {
            try
            {
                var affected = await _repository.DeleteById(id);
                if (affected is not 1)
                {
                    return NotFound(new { msg = "Data NotFound" });
                }
                return Ok(new { msg = $"affected_rows {affected}" });
            }
            catch (Exception ex)
            {
                if (ex.Message is "Data NotFound")
                    return NotFound(new { msg = "Data NotFound" });
                else
                    return BadRequest(new { msg = ex.Message });
            }
        }
    }
}
