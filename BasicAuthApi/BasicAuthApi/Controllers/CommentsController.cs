﻿using BasicAuthApi.Models;
using BasicAuthApi.Repository.Interfaces;
using BasicAuthApi.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BasicAuthApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CommentsController : ControllerBase
    {
        private readonly ICommentsRepository _repository;

        public CommentsController(ICommentsRepository repository)
        {
            _repository = repository;
        }

        // GET: api/<CommentsController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Comments>>> Get()
        {
            try
            {
                var data = await _repository.GetAll();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(new { msg = ex.Message });
            }
        }

        // GET api/<CommentsController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Comments>> Get(int id)
        {
            try
            {
                var data = await _repository.GetById(id);

                if (data is null)
                {
                    return NotFound(new { msg = "Data NotFound" });
                }

                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(new { msg = ex.Message });
            }
        }

        // GET api/<CommentsController>/GetByPost/1
        [HttpGet]
        [Route("GetByPost/{idPost}")]
        public async Task<ActionResult<Comments>> GetByPost(int idPost)
        {
            try
            {
                var data = await _repository.GetByPost(idPost);

                if (data is null)
                {
                    return NotFound(new { msg = "Data NotFound" });
                }

                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(new { msg = ex.Message });
            }
        }

        // POST api/<CommentsController>
        [HttpPost]
        public async Task<ActionResult<int>> Post([FromBody] Comments comments)
        {
            try
            {
                var affected = await _repository.Upsert(comments);
                if (affected is not 1)
                {
                    return NotFound(new { msg = "Data NotFound" });
                }
                return Ok(new { msg = $"affected_rows {affected}" });
            }
            catch (Exception ex)
            {
                return BadRequest(new { msg = ex.Message });
            }
        }

        // PUT api/<CommentsController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult<int>> Put(int id, [FromBody] Comments comments)
        {
            try
            {
                comments.Id = id;
                var affected = await _repository.Upsert(comments);
                if (affected is not 1)
                {
                    return NotFound(new { msg = "Data NotFound" });
                }
                return Ok(new { msg = $"affected_rows {affected}" });
            }
            catch (Exception ex)
            {
                if (ex.Message is "Data NotFound")
                    return NotFound(new { msg = "Data NotFound" });
                else
                    return BadRequest(new { msg = ex.Message });
            }
        }

        // POST api/<CommentsController>/Populate
        [HttpPost]
        [Route("Populate")]
        public ActionResult Populate()
        {
            try
            {
                // Get all comments
                var urlBase = "https://jsonplaceholder.typicode.com";
                var endpoint = "/comments";
                var headers = new Dictionary<string, string>()
                {
                    {"Accept", "application/json"},
                    {"Content-Type", "application/json"},
                };
                var comments = new MyHttp<List<Comments>>(urlBase, endpoint, headers).Get();

                if (comments is null)
                {
                    return NotFound(new { msg = "Data NotFound" });
                }

                // Insert comments
                foreach (var comment in comments)
                {
                    _repository.Upsert(comment);
                }

                return Ok(new { msg = "ok" });
            }
            catch (Exception ex)
            {
                return BadRequest(new { msg = ex.Message });
            }
        }

        // DELETE api/<CommentsController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<int>> Delete(int id)
        {
            try
            {
                var affected = await _repository.DeleteById(id);
                if (affected is not 1)
                {
                    return NotFound(new { msg = "Data NotFound" });
                }
                return Ok(new { msg = $"affected_rows {affected}" });
            }
            catch (Exception ex)
            {
                if (ex.Message is "Data NotFound")
                    return NotFound(new { msg = "Data NotFound" });
                else
                    return BadRequest(new { msg = ex.Message });
            }
        }
    }
}
