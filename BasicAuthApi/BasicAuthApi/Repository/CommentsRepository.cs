﻿using BasicAuthApi.Models;
using BasicAuthApi.Repository.Interfaces;
using Dapper;
using System.Data;
using System.Data.SqlClient;

namespace BasicAuthApi.Repository
{
    public class CommentsRepository : ICommentsRepository
    {
        private readonly IConfiguration _config;
        private readonly string _strConn;

        public CommentsRepository(IConfiguration config)
        {
            _config = config;
            _strConn = _config.GetConnectionString("DefaultConnection");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="comments"></param>
        /// <returns></returns>
        public async Task<int> Upsert(Comments comments)
        {
            using var conn = new SqlConnection(_strConn);
            return await conn.ExecuteAsync("CommUpsert", comments, commandType: CommandType.StoredProcedure);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Comments>> GetAll()
        {
            using var conn = new SqlConnection(_strConn);
            return await conn.QueryAsync<Comments>("CommGetAll", commandType: CommandType.StoredProcedure);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Comments> GetById(int id)
        {
            using var conn = new SqlConnection(_strConn);
            return await conn.QueryFirstOrDefaultAsync<Comments>("CommGetById", new { Id = id }, commandType: CommandType.StoredProcedure);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idPost"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Comments>> GetByPost(int idPost)
        {
            using var conn = new SqlConnection(_strConn);
            return await conn.QueryAsync<Comments>("CommGetByPost", new { IdPost = idPost }, commandType: CommandType.StoredProcedure);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<int> DeleteById(int id)
        {
            using var conn = new SqlConnection(_strConn);
            return await conn.ExecuteAsync("CommDeleteById", new { Id = id }, commandType: CommandType.StoredProcedure);
        }
    }
}
