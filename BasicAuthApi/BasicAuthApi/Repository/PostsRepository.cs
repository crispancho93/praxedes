﻿using BasicAuthApi.Models;
using BasicAuthApi.Repository.Interfaces;
using Dapper;
using System.Data;
using System.Data.SqlClient;

namespace BasicAuthApi.Repository
{
    public class PostsRepository : IPostsRepository
    {
        private readonly IConfiguration _config;
        private readonly string _strConn;

        public PostsRepository(IConfiguration config)
        {
            _config = config;
            _strConn = _config.GetConnectionString("DefaultConnection");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        public async Task<int> Upsert(Posts post)
        {
            using var conn = new SqlConnection(_strConn);
            return await conn.ExecuteAsync("PostsUpsert", post, commandType: CommandType.StoredProcedure);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Posts>> GetAll()
        {
            using var conn = new SqlConnection(_strConn);
            return await conn.QueryAsync<Posts>("PostsGetAll", commandType: CommandType.StoredProcedure);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Posts> GetById(int id)
        {
            using var conn = new SqlConnection(_strConn);
            return await conn.QueryFirstOrDefaultAsync<Posts>("PostsGetById", new { Id = id }, commandType: CommandType.StoredProcedure);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<int> DeleteById(int id)
        {
            using var conn = new SqlConnection(_strConn);
            return await conn.ExecuteAsync("PostsDeleteById", new { Id = id }, commandType: CommandType.StoredProcedure);
        }
    }
}
