﻿using BasicAuthApi.Models;

namespace BasicAuthApi.Repository.Interfaces
{
    public interface ICommentsRepository
    {
        Task<int> Upsert(Comments comments);
        Task<IEnumerable<Comments>> GetAll();
        Task<Comments> GetById(int id);
        Task<IEnumerable<Comments>> GetByPost(int idPost);
        Task<int> DeleteById(int id);
    }
}
