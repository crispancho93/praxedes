﻿using BasicAuthApi.Models;

namespace BasicAuthApi.Repository.Interfaces
{
    public interface IPostsRepository
    {
        Task<int> Upsert(Posts posts);
        Task<IEnumerable<Posts>> GetAll();
        Task<Posts> GetById(int id);
        Task<int> DeleteById(int id);
    }
}
