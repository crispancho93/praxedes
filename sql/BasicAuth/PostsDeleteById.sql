USE [BasicAuth]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE OR ALTER PROCEDURE PostsDeleteById
	-- Add the parameters for the stored procedure here
	 @Id INTEGER
AS
BEGIN

    -- Insert statements for procedure here
	DELETE FROM Posts WHERE Id = @Id
END
GO
