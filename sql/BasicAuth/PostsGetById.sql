USE [BasicAuth]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE OR ALTER PROCEDURE PostsGetById
	@Id INT
AS
BEGIN

    -- Insert statements for procedure here
	SELECT * FROM Posts WHERE Id = @Id
END
GO
