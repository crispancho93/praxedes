USE [BasicAuth]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE OR ALTER PROCEDURE PostsUpsert
	-- Add the parameters for the stored procedure here
	 @Id INTEGER
	,@UserId INT
	,@Title VARCHAR(45)
	,@Body VARCHAR(MAX)
AS
BEGIN

    -- Insert statements for procedure here
	IF NOT EXISTS (SELECT * FROM Posts WHERE Id = @Id)
		INSERT INTO [dbo].[Posts]
		(	
			 [Id]
			,[UserId]
			,[Title]
			,[Body]
		)
		VALUES
		(
			 @Id
			,@UserId
			,@Title
			,@Body
		)

	ELSE
        UPDATE [dbo].[Posts]
        SET [UserId] = @UserId
            ,[Title] = @Title
            ,[Body] = @Body
        WHERE Id = @Id
END
GO
