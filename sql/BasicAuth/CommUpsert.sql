USE [BasicAuth]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE OR ALTER PROCEDURE CommUpsert
	-- Add the parameters for the stored procedure here
	 @Id INTEGER
	,@PostId INTEGER
	,@Name VARCHAR(45)
	,@Email VARCHAR(45)
	,@Body VARCHAR(MAX)
AS
BEGIN

    -- Insert statements for procedure here
	IF NOT EXISTS (SELECT * FROM Comments WHERE Id = @Id)
		INSERT INTO [dbo].[Comments]
		(	
			 [Id]
			,[PostId]
			,[Name]
			,[Email]
			,[Body]
		)
		VALUES
		(
			 @Id
			,@PostId
			,@Name
			,@Email
			,@Body
		)

	ELSE
        UPDATE [dbo].[Comments]
		SET [PostId] = @PostId
			,[Name] = @Name
			,[Email] = @Email
			,[Body] = @Body
		WHERE Id = @Id
END
GO
